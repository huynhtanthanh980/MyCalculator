package com.testing.kmm.mycalculator;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener, java.lang.Runnable {

    Button MainActivity_btn_send_press_0, MainActivity_btn_send_press_1, MainActivity_btn_send_press_2, MainActivity_btn_send_press_3, MainActivity_btn_send_press_4, MainActivity_btn_send_press_5, MainActivity_btn_send_press_6, MainActivity_btn_send_press_7, MainActivity_btn_send_press_8, MainActivity_btn_send_press_9;
    Button MainActivity_btn_send_press_Plus, MainActivity_btn_send_press_Minus, MainActivity_btn_send_press_Multiply, MainActivity_btn_send_press_Divide, MainActivity_btn_send_press_Power;
    Button MainActivity_btn_send_press_AC, MainActivity_btn_send_press_Delete, MainActivity_btn_send_press_Parenthesis, MainActivity_btn_send_press_Dot, MainActivity_btn_send_press_execute;

    private TextView MainActivity_txt_Answer;

    private EditText MainActivity_txt_Input;

    private double CurrentAnswer = 0.0;
    private double PreviousAnswer = 0.0; //to store CurrentAnswer after AC
    private int parenthesisCount; //check number of parenthesis
    private int DebugModeCount = 0;

    private Handler handler = new Handler();

    private boolean AutoDeleteFlag = false;
    private int DeleteDelayTime = 100;

    private int InputCursorPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        MainActivity_btn_send_press_0 = findViewById(R.id.MainActivity_btn_send_press_0);
        MainActivity_btn_send_press_0.setOnClickListener(this);
        MainActivity_btn_send_press_0.setOnLongClickListener(this);

        MainActivity_btn_send_press_1 = findViewById(R.id.MainActivity_btn_send_press_1);
        MainActivity_btn_send_press_1.setOnClickListener(this);
        MainActivity_btn_send_press_1.setOnLongClickListener(this);

        MainActivity_btn_send_press_2 = findViewById(R.id.MainActivity_btn_send_press_2);
        MainActivity_btn_send_press_2.setOnClickListener(this);
        MainActivity_btn_send_press_2.setOnLongClickListener(this);

        MainActivity_btn_send_press_3 = findViewById(R.id.MainActivity_btn_send_press_3);
        MainActivity_btn_send_press_3.setOnClickListener(this);
        MainActivity_btn_send_press_3.setOnLongClickListener(this);

        MainActivity_btn_send_press_4 = findViewById(R.id.MainActivity_btn_send_press_4);
        MainActivity_btn_send_press_4.setOnClickListener(this);
        MainActivity_btn_send_press_4.setOnLongClickListener(this);

        MainActivity_btn_send_press_5 = findViewById(R.id.MainActivity_btn_send_press_5);
        MainActivity_btn_send_press_5.setOnClickListener(this);
        MainActivity_btn_send_press_5.setOnLongClickListener(this);

        MainActivity_btn_send_press_6 = findViewById(R.id.MainActivity_btn_send_press_6);
        MainActivity_btn_send_press_6.setOnClickListener(this);
        MainActivity_btn_send_press_6.setOnLongClickListener(this);

        MainActivity_btn_send_press_7 = findViewById(R.id.MainActivity_btn_send_press_7);
        MainActivity_btn_send_press_7.setOnClickListener(this);
        MainActivity_btn_send_press_7.setOnLongClickListener(this);

        MainActivity_btn_send_press_8 = findViewById(R.id.MainActivity_btn_send_press_8);
        MainActivity_btn_send_press_8.setOnClickListener(this);
        MainActivity_btn_send_press_8.setOnLongClickListener(this);

        MainActivity_btn_send_press_9 = findViewById(R.id.MainActivity_btn_send_press_9);
        MainActivity_btn_send_press_9.setOnClickListener(this);
        MainActivity_btn_send_press_9.setOnLongClickListener(this);

        MainActivity_btn_send_press_Plus = findViewById(R.id.MainActivity_btn_send_press_Plus);
        MainActivity_btn_send_press_Plus.setOnClickListener(this);
        MainActivity_btn_send_press_Plus.setOnLongClickListener(this);

        MainActivity_btn_send_press_Minus = findViewById(R.id.MainActivity_btn_send_press_Minus);
        MainActivity_btn_send_press_Minus.setOnClickListener(this);
        MainActivity_btn_send_press_Minus.setOnLongClickListener(this);

        MainActivity_btn_send_press_Multiply = findViewById(R.id.MainActivity_btn_send_press_Multiply);
        MainActivity_btn_send_press_Multiply.setOnClickListener(this);
        MainActivity_btn_send_press_Multiply.setOnLongClickListener(this);

        MainActivity_btn_send_press_Divide = findViewById(R.id.MainActivity_btn_send_press_Divide);
        MainActivity_btn_send_press_Divide.setOnClickListener(this);
        MainActivity_btn_send_press_Divide.setOnLongClickListener(this);

        MainActivity_btn_send_press_Power = findViewById(R.id.MainActivity_btn_send_press_Power);
        MainActivity_btn_send_press_Power.setOnClickListener(this);
        MainActivity_btn_send_press_Power.setOnLongClickListener(this);

        MainActivity_btn_send_press_AC = findViewById(R.id.MainActivity_btn_send_press_AC);
        MainActivity_btn_send_press_AC.setOnClickListener(this);
        MainActivity_btn_send_press_AC.setOnLongClickListener(this);

        MainActivity_btn_send_press_Delete = findViewById(R.id.MainActivity_btn_send_press_Delete);
        MainActivity_btn_send_press_Delete.setOnClickListener(this);
        MainActivity_btn_send_press_Delete.setOnLongClickListener(this);
        MainActivity_btn_send_press_Delete.setOnTouchListener(this);

        MainActivity_btn_send_press_Parenthesis = findViewById(R.id.MainActivity_btn_send_press_Parenthesis);
        MainActivity_btn_send_press_Parenthesis.setOnClickListener(this);
        MainActivity_btn_send_press_Parenthesis.setOnLongClickListener(this);

        MainActivity_btn_send_press_Dot = findViewById(R.id.MainActivity_btn_send_press_Dot);
        MainActivity_btn_send_press_Dot.setOnClickListener(this);
        MainActivity_btn_send_press_Dot.setOnLongClickListener(this);

        MainActivity_btn_send_press_execute = findViewById(R.id.MainActivity_btn_send_press_Equal);
        MainActivity_btn_send_press_execute.setOnClickListener(this);
        MainActivity_btn_send_press_execute.setOnLongClickListener(this);


        MainActivity_txt_Input = findViewById(R.id.MainActivity_txt_Input);
        MainActivity_txt_Input.setSelectAllOnFocus(false);
        MainActivity_txt_Input.setOnTouchListener(this);
        MainActivity_txt_Input.setShowSoftInputOnFocus(false);
        setInputSelection(0);
        MainActivity_txt_Input.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                execute();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        MainActivity_txt_Answer = findViewById(R.id.MainActivity_txt_Answer);

    }

    protected int operatorPrecedence(char a) {
        switch (a) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '^':
                return 3;
            default:
                return 0;
        }
    }

    protected int getInputSelectionStart() {
        return MainActivity_txt_Input.getSelectionStart();
    }

    protected int getInputSelectionEnd() {
        return MainActivity_txt_Input.getSelectionEnd();
    }

    protected void setInputSelection(int index) {
        MainActivity_txt_Input.setSelection(index);
    }

    protected String getInputText() {
        return MainActivity_txt_Input.getText().toString();
    }

    protected void setInputViewText(String InputString) {
        MainActivity_txt_Input.setText(InputString);
    }

    protected void setAnswerViewText(String InputString) {
        MainActivity_txt_Answer.setText(InputString);
    }

    protected String toPostFix() {

        String InputString = getInputText();
        
        String Equation = "";

        Stack<Character> SignStack = new Stack<>();

        boolean NextToSignFlag = true; //if true then there is a sign before the number
        boolean HadDotFlag = false; //if true then there is a dot in the number

        for (int CharacterCount = 0; CharacterCount < InputString.length(); CharacterCount++) {
            char CurrentCharacter = InputString.charAt(CharacterCount);

            if (NextToSignFlag && CurrentCharacter == '-') {
                Equation += CurrentCharacter;
                NextToSignFlag = false;
            }

            else if (Character.isLetterOrDigit(CurrentCharacter) || CurrentCharacter == '.') {
                if (CurrentCharacter == '.') {
                    if (HadDotFlag) return "E";
                    else HadDotFlag = true;
                }
                Equation += CurrentCharacter;
                NextToSignFlag = false;
            }

            else if (CurrentCharacter == '(') {
                if ( CharacterCount > 0 && InputString.charAt(CharacterCount - 1) != '+' && InputString.charAt(CharacterCount - 1) != '-' && InputString.charAt(CharacterCount - 1) != '*' && InputString.charAt(CharacterCount - 1) != '/' && InputString.charAt(CharacterCount - 1) != '^') {
                    SignStack.push('*');
                    Equation += 'x';
                }
                SignStack.push(CurrentCharacter);
            }

            else if (CurrentCharacter == ')') {
                while (!SignStack.isEmpty() && SignStack.peek() != '(') {
                    Equation += 'x';
                    Equation += SignStack.pop();
                }

                if (!SignStack.isEmpty() && SignStack.peek() != '(') return "E";

                else SignStack.pop();
            }

            else {
                while (!SignStack.isEmpty() && operatorPrecedence(CurrentCharacter) <= operatorPrecedence(SignStack.peek())) {
                    Equation += 'x';
                    Equation += SignStack.pop();
                }
                Equation += 'x';
                SignStack.push(CurrentCharacter);
                NextToSignFlag = true;
                HadDotFlag = false;
            }
        }

        while(!SignStack.isEmpty()) {
            Equation += 'x';
            Equation += SignStack.pop();
        }

        return Equation;
    }

    protected void input(char InputCharacter) {
        InputCursorPosition = getInputSelectionStart() + 1;
        setInputViewText(getInputText().substring(0, getInputSelectionStart()) + InputCharacter + getInputText().substring(getInputSelectionEnd())); //set the MainActivity_txt_Input display
        setInputSelection(InputCursorPosition);
        execute(); //execute after every input to calculate immediately
    }

    protected double CalculateResult(double FirstNumber, char Sign, double SecondNumber) {
        double result = 0;
        switch (Sign) {
            case '+':
                result = FirstNumber + SecondNumber;
                break;
            case '-':
                result =  FirstNumber - SecondNumber;
                break;
            case '*':
                result =  FirstNumber * SecondNumber;
                break;
            case '/':
                result =  FirstNumber / SecondNumber;
                break;
            case '^':
                result =  Math.pow(FirstNumber, SecondNumber);
                break;
        }
        return result;
    }

    protected boolean HaveError(String InputString) {
        if (InputString.equals("E")) {
            setAnswerViewText("Error!");
            return true;
        }
        return false;
    }
    
    protected double ToDouble(String StringNumber) {
        return Double.parseDouble(StringNumber);
    }
    
    protected boolean IsSpaceMark(char inputChar) {
        return inputChar == 'x';
    }
    
    protected boolean IsSign(char inputChar) {
        return inputChar == '+' || inputChar == '-' || inputChar == '*' || inputChar == '/' || inputChar == '^';
    }

    protected void execute() {

        Stack<Double> stack = new Stack<>();

        String PostFixString = toPostFix();

        if (HaveError(PostFixString)) return;

        //if (DebugModeCount == 5) setInputViewText(inputString + "  |  " + PostFixString);

        String number = "";

        for (int CharacterCount = 0; CharacterCount < PostFixString.length(); CharacterCount++) {

            char a = PostFixString.charAt(CharacterCount);

            if (IsSpaceMark(a)) {
                if (!number.equals("") && !number.equals("-")) {
                    stack.push(ToDouble(number));
                    number = "";
                }
            }

            else if (IsSign(a)) {
                if (number.isEmpty() && a == '-' && CharacterCount < (PostFixString.length() - 1) && Character.isDigit(PostFixString.charAt(CharacterCount + 1))) number += a;
                else if (stack.isEmpty() || stack.size() == 1) {
                    setAnswerViewText("0");
                    return;
                }
                else if (stack.size() > 1) {
                    double first = stack.pop();
                    if ((a == '/' && first == 0) || (a == '^' && stack.peek() < 0 && Math.abs(first) < 1 && Math.abs(first) > 0 && (1/first) % 2 == 0)) {
                        setAnswerViewText("Error!");
                        return;
                    }
                    else stack.push(CalculateResult(stack.pop(), a, first));
                }
            }

            else if(a != '(') number += a;
        }

        if (stack.isEmpty() && (number.isEmpty() || number.equals("-") || number.equals("."))) return;
        else if (stack.isEmpty()) CurrentAnswer = ToDouble(number);
        else CurrentAnswer = stack.pop();

        String result = convertToResult(CurrentAnswer);

        if (result.contains("E")) {
            if (result.length() > 10) result = result.substring(0, 7) + "x10^" + result.substring(result.indexOf("E") + 1);
            else result = result.substring(0, result.indexOf("E")) + "x10^" + result.substring(result.indexOf("E") + 1);
        }

        setAnswerViewText(result);
    }

    protected String convertToResult(double answer) {
        int num = (int) answer;

        if (Math.abs(answer - (double) num) < 0.0000000001) {
            answer = num;
            return Integer.toString(num);
        } else // print as double
        {
            return Double.toString(answer);
        }
    }

    protected int countChar(String a, char c) {
        int count = 0;
        for (char ch : a.toCharArray()) {
            if (ch == c) count++;
        }
        return count;
    }

    protected void delete() {
        if (MainActivity_txt_Input.length() > 0) {
            if (getInputSelectionStart() > 0 && getInputSelectionStart() == getInputSelectionEnd()) {
                int selectionStart = getInputSelectionStart();
                int selectionEnd = getInputSelectionEnd();
                int deletedParenthesisCount = 0;
                InputCursorPosition = getInputSelectionStart() - 1;
                if (getInputSelectionStart() > 0 && MainActivity_txt_Input.getText().charAt(getInputSelectionStart() - 1) == ')')
                    parenthesisCount++;
                else if (getInputSelectionStart() > 0 && MainActivity_txt_Input.getText().charAt(getInputSelectionStart() - 1) == '(') {
                    parenthesisCount--;
                    if (parenthesisCount < 0)
                        for (int i = getInputSelectionStart() - 1; i < MainActivity_txt_Input.length(); i++) {
                             if (MainActivity_txt_Input.getText().charAt(i) == ')') {
                                 if (i == MainActivity_txt_Input.length() - 1)
                                     setInputViewText(getInputText().substring(0, getInputSelectionStart() - 1) + getInputText().substring(getInputSelectionEnd(), i));
                                 else
                                     setInputViewText(getInputText().substring(0, getInputSelectionStart() - 1) + getInputText().substring(getInputSelectionEnd(), i) + getInputText().substring(i + 1));
                                 deletedParenthesisCount++;
                                 parenthesisCount++;
                                 if (parenthesisCount == 0) break;
                             }
                         }
                }
                if (deletedParenthesisCount == 0)
                    setInputViewText(getInputText().substring(0, selectionStart - 1) + getInputText().substring(selectionEnd));
            } else if (getInputSelectionStart() != getInputSelectionEnd()) {
                int selectionStart = getInputSelectionStart();
                int selectionEnd = getInputSelectionEnd();
                InputCursorPosition = getInputSelectionStart();
                parenthesisCount += countChar(getInputText().substring(getInputSelectionStart(), getInputSelectionEnd()), ')');
                parenthesisCount -= countChar(getInputText().substring(getInputSelectionStart(), getInputSelectionEnd()), '(');
                if (parenthesisCount < 0) {
                    int i;
                    if (getInputSelectionEnd() == 0) i = 0;
                    else i = getInputSelectionEnd() - 1;
                    for (; i < MainActivity_txt_Input.length(); i++) {
                        if (MainActivity_txt_Input.getText().charAt(i) == ')') {
                            if (i == MainActivity_txt_Input.length() - 1)
                                setInputViewText(getInputText().substring(0, i));
                            else
                                setInputViewText(getInputText().substring(0, i) + getInputText().substring(i + 1));
                            parenthesisCount++;
                            if (parenthesisCount == 0) break;
                            i--;
                        }
                    }
                }
                setInputViewText(getInputText().substring(0, selectionStart - 1) + getInputText().substring(selectionEnd));
            }
            setInputSelection(InputCursorPosition);
        }
        if (MainActivity_txt_Input.length() == 0) {
            CurrentAnswer = 0;
            setAnswerViewText("0");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.MainActivity_btn_send_press_0:
                input('0');
                break;

            case R.id.MainActivity_btn_send_press_1:
                input('1');
                break;

            case R.id.MainActivity_btn_send_press_2:
                input('2');
                break;

            case R.id.MainActivity_btn_send_press_3:
                input('3');
                break;

            case R.id.MainActivity_btn_send_press_4:
                input('4');
                break;

            case R.id.MainActivity_btn_send_press_5:
                input('5');
                break;

            case R.id.MainActivity_btn_send_press_6:
                input('6');
                break;

            case R.id.MainActivity_btn_send_press_7:
                input('7');
                break;

            case R.id.MainActivity_btn_send_press_8:
                input('8');
                break;

            case R.id.MainActivity_btn_send_press_9:
                input('9');
                break;

            case R.id.MainActivity_btn_send_press_Plus:
                input('+');
                break;

            case R.id.MainActivity_btn_send_press_Minus:
                input('-');
                break;

            case R.id.MainActivity_btn_send_press_Multiply:
                input('*');
                break;

            case R.id.MainActivity_btn_send_press_Divide:
                input('/');
                break;

            case R.id.MainActivity_btn_send_press_Power:
                input('^');
                break;

            case R.id.MainActivity_btn_send_press_AC:
                PreviousAnswer = CurrentAnswer;
                CurrentAnswer = 0.0;
                setInputSelection(0);
                setInputViewText("");
                setAnswerViewText("0");
                parenthesisCount = 0;
                DebugModeCount = 0;
                InputCursorPosition = 0;
                break;

            case R.id.MainActivity_btn_send_press_Delete:
                delete();
                break;

            case R.id.MainActivity_btn_send_press_Parenthesis:
                if (parenthesisCount == 0 || (MainActivity_txt_Input.length() > 0 && MainActivity_txt_Input.getText().charAt(MainActivity_txt_Input.length() - 1) == '(')) {
                    input('(');
                    parenthesisCount++;
                } else if (parenthesisCount > 0) {
                    input(')');
                    parenthesisCount--;
                }
                break;

            case R.id.MainActivity_btn_send_press_Dot:
                input('.');
                break;

            case R.id.MainActivity_btn_send_press_Equal:
                parenthesisCount = 0;
                String result = convertToResult(CurrentAnswer);
                if (result.contains("E")) {
                    result = result.substring(0, result.indexOf("E")) + "*10^" + result.substring(result.indexOf("E") + 1);
                }
                setInputViewText(result);
                setInputSelection(result.length());
                execute();
                break;

            default:
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.MainActivity_btn_send_press_Equal:
                parenthesisCount = 0;
                int a = getInputSelectionEnd();
                String result = convertToResult(PreviousAnswer);
                setInputViewText(getInputText().substring(0, getInputSelectionStart()) + result + getInputText().substring(getInputSelectionEnd()));
                setInputSelection(a + result.length());
                execute();
                break;

            case R.id.MainActivity_btn_send_press_Parenthesis:
                input('(');
                parenthesisCount++;
                break;

            case R.id.MainActivity_btn_send_press_AC:
                DebugModeCount = 1;
                Toast.makeText(this, "4", Toast.LENGTH_SHORT).show();
                break;

            case R.id.MainActivity_btn_send_press_0:
                if (DebugModeCount == 1) {
                    DebugModeCount = 2;
                    Toast.makeText(this, "3", Toast.LENGTH_SHORT).show();
                }
                else DebugModeCount = 0;
                break;

            case R.id.MainActivity_btn_send_press_9:
                if (DebugModeCount == 2) {
                    DebugModeCount = 3;
                    Toast.makeText(this, "2", Toast.LENGTH_SHORT).show();
                }
                else DebugModeCount = 0;
                break;

            case R.id.MainActivity_btn_send_press_1:
                if (DebugModeCount == 3) {
                    DebugModeCount = 4;
                    Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();
                }
                else DebugModeCount = 0;
                break;

            case R.id.MainActivity_btn_send_press_8:
                if (DebugModeCount == 4) {
                    DebugModeCount = 5;
                    Toast.makeText(this, "Debug Mode: On", Toast.LENGTH_SHORT).show();
                }
                else DebugModeCount = 0;
                break;

            case R.id.MainActivity_btn_send_press_Delete:
                AutoDeleteFlag = true;
                handler.postDelayed(this, DeleteDelayTime);
                break;

            default:
                DebugModeCount = 0;
                break;
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.MainActivity_btn_send_press_Delete:
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && AutoDeleteFlag) {
                    AutoDeleteFlag = false;
                }
                break;
            case R.id.MainActivity_txt_Input:
                if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    MainActivity_txt_Input.post(new Runnable()
                    {
                        public void run()
                        {
                            InputCursorPosition = getInputSelectionStart();
                            setInputSelection(InputCursorPosition);
                        }
                    });
                }
                break;
        }
        return false;
    }

    @Override
    public void run() {
        if (AutoDeleteFlag) {
            delete();
            handler.postDelayed(this, DeleteDelayTime);
        }
    }
}
